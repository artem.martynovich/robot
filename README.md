# Robot

Refer to READMEs in the subfolders:
* [pi/](pi) - The code running on Raspberry Pi,
* [cloud/](cloud) - The code running on a cloud server,
* [web/](web) - The web frontend.

## How to generate SSL certs

Follow $1773831 to generate CA key+certificate and SSL key+certificate. Install the CA certificate on Raspberry Pi, on the cloud server and on all user devices which will connect to the web server. This will make any SSL certificate signed by this CA automatically trusted.