#!/usr/bin/env python3
import argparse
import asyncio
import json
import websockets
import ssl
import time

import pibot

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject

BOT = pibot.PiBot()

def play_sound():
    PIPELINE='''
	filesrc location=/opt/pi-robot/sounds/running.wav ! 
	wavparse ! audioconvert ! 
	audioresample ! audio/x-raw,rate=48000,channels=2,format=S16LE ! 
	alsasink device=hw:1'''
    pipe = Gst.parse_launch(PIPELINE)
    BOT.Enable()
    pipe.set_state(Gst.State.PLAYING)
    b = pipe.get_bus()
    msg = b.timed_pop_filtered(Gst.CLOCK_TIME_NONE, Gst.MessageType.ERROR|Gst.MessageType.EOS)
    pipe.set_state(Gst.State.NULL)
    print("sound was played")
    pibot.PiBot.Disable()

class RobotCamera(object):
    COMMANDS = ['resolution', 'bitrate', 'look'] 
    LOCALHOST = '127.0.0.1'

    CAPS = 'video/x-h264, width={width}, height={height}, framerate=(fraction)30/1, profile=(string)baseline'
    PIPELINE = '''
      v4l2src blocksize=400000  device=/dev/video0 !
        {CAPS} !
        queue leaky=2 !
        h264parse !
        rtph264pay config-interval=1 pt=96  perfect-rtptime=true min-ptime=10 seqnum-offset=0 timestamp-offset=0 !
        udpsink host={LOCALHOST} port={video_rtp_port} buffer-size=32768 sync=false async=false
      alsasrc device=hw:{audio_in_device} buffer-time=40000 !
        queue leaky=2 !
        audioconvert !
        audio/x-raw, rate=48000, channels=2 !
        opusenc bitrate=128000 inband-fec=true frame-size=10 !
        opusparse !
        rtpopuspay pt=111 perfect-rtptime=true min-ptime=10 seqnum-offset=0 timestamp-offset=0 !
        udpsink host={LOCALHOST} port={audio_rtp_out_port} sync=false async=false
      udpsrc port={audio_rtp_in_port} !
        application/x-rtp,media=audio,payload=111,encoding-name=OPUS !
        queue leaky=2 !
        rtpopusdepay !
        opusparse !
        opusdec !
        audiomixer name=mix !
        alsasink buffer-time=40000 device=hw:{audio_out_device}
      filesrc location=/opt/pi-robot/sounds/client_connected.wav ! 
        wavparse ! audioconvert ! audioresample ! audio/x-raw,rate=48000,channels=2,format=S16LE ! mix.
    '''

    def __init__(self, kwargs):
        self._enabled = False
        Gst.init(None)
        GObject.threads_init()
        self.pipe = Gst.parse_launch(
            self.PIPELINE.format(**kwargs,
                CAPS=self.CAPS,
                LOCALHOST=self.LOCALHOST)
            .format(**kwargs)
        )
        self.caps0 = self.pipe.get_by_name('capsfilter0')
        self.src0 = self.pipe.get_by_name('v4l2src0')
        if self.caps0 is None or self.src0 is None:
            print('Could not get self.caps0 or src0')

    def set_v4l_size(self, W, H):
        newcaps = Gst.Caps.from_string(self.CAPS.format(width=W, height=H))
        self.caps0.set_property('caps', newcaps)

    def set_v4l_bitrate(self, kb):
        self.src0.set_property('extra-controls',
                               Gst.Structure.from_string('c,video_bitrate={}'.format(kb*1000))[0])

    def handle(self, cmd, obj):
        if cmd == 'resolution':
            self.set_v4l_size(obj['width'], obj['height'])
        elif cmd == 'bitrate':
            self.set_v4l_bitrate(obj['bitrate'])

    def enable(self):
        print('camera: ENABLE')
        if self._enabled:
            return
        r = self.pipe.set_state(Gst.State.PLAYING)
        print('Playing (again) the pipeline returned '+r.value_name)
        # TODO: error handling
        self._enabled = True

    def disable(self):
        print('camera: DISABLE')
        if not self._enabled:
            return
        r = self.pipe.set_state(Gst.State.NULL)
        print('Pausing the pipeline returned '+r.value_name)
        self._enabled = False

    @property
    def commands(self):
        return self.COMMANDS

class Step():

    loop = asyncio.get_event_loop()

    def __init__(self, initial, cb):
        self._interval = None
        self._initial = initial
        self._current = initial
        self.cur = initial
        self.delay = 0
        self.stepval = 0
        self.to = initial
        self.cb = cb

    def step(self, to, step, delay):        
        self._step(self._current, to, step, delay)
    
    def cancel(self, reset=False):
        if self._interval:
            self._interval.cancel()
        if reset:
            self._current = self._initial
            self.cur = self._initial

    def _cb(self, v):
        self._current = v
        self.cb(v)
    
    def _timerFunc(self):
        if self.increasing:
            self.cur += self.stepval
            stop = self.cur > self.to
        else:
            self.cur -= self.stepval
            stop = self.cur < self.to

        if stop:
            self._interval = None
            self._cb(self.to)
        else:
            self._cb(self.cur)
            self._interval = self.loop.call_later(self.delay, self._timerFunc)
            
    def _step(self, _from, _to, _step, _delay):
        if self._interval:
            self._interval.cancel()
        
        self.cur = _from
        self.stepval = _step
        self.to = _to
        self.delay = _delay
        if _to > _from:
            if _to - _from < _step:
                return self._cb(_to)
            self.increasing = True
            self._timerFunc()
        else:
            if _from - _to < _step:
                return self._cb(_to)
            self.increasing = False
            self._timerFunc()

class RobotJoystick(object):
    COMMANDS = ['move', 'start', 'end', 'look', 'wheel']
    MINTIME = 0.1

    SERVO_H = 1
    SERVO_H_MAX = 180
    SERVO_H_MIN = 0
    
    SERVO_V = 2
    SERVO_V_MAX = 180
    SERVO_V_MIN = 0

    COMMAND_SERVO_PLAIN = {'h': SERVO_H, 'v': SERVO_V}

    STEP = 0.05
    DELAY = 0.05
    LOOK_DELAY = 0.1
    

    def angle_to_t(a):
        tMin = 130.
        tMax = 540.
        t = (a/180.)*(tMax-tMin)+tMin
        res = int(t)
        #res = res - (res % 16)
        print("angle_to_t: {}".format(res))
        return res

    def __init__(self):
        self._enabled = False
        self.bot = BOT
        self.bot.InitMotorDriver(pibot.DRIVER_M_1_2)
        self.enable()

        self.step_left = Step(0, lambda v: 
            self.setMotor(pibot.M1, v))
        self.step_right = Step(0, lambda v: 
            self.bot.SetMotorDrive(pibot.M2, int(v*255)))
        self.step_look_h = Step(0, lambda v: 
            self.setServo(self.SERVO_H, v))
        self.step_look_v = Step(0, lambda v: 
            self.setServo(self.SERVO_V, v))
        
        self.disable(False)
    
    def setMotor(self, m, v):
        if self._enabled:
            self.bot.SetMotorDrive(m, int(v*255))
        
    def setServo(self, s, v):
        if self._enabled:
            self.bot.SetServoControl(s, RobotJoystick.angle_to_t(int(v + 90)))

    def move(self, force, angle):
        if force > 1.0:
            force = 1.0
        if angle == 360:
            angle = 0

        m1 = force
        m2 = force - (force*2)*(angle%90)/90.
        if 0 <= angle < 90:
            M1 = m1
            M2 = m2
        elif 90 <= angle < 180:
            M1 = m2            
            M2 = -m1
        elif 180 <= angle < 270:
            M1 = -m1
            M2 = -m2
        elif 270 <= angle < 360:
            M1 = -m2
            M2 = m1

        self.step_left.step(M1, self.STEP, self.DELAY)
        self.step_right.step(M2, self.STEP, self.DELAY)

    def look(self, plain, force):
        if force < 0:
            maxangle = -90
            force = -force
        else:
            maxangle = 90
        if plain == 'h':
            self.step_look_v.cancel()
            self.step_look_h.step(maxangle, force, self.LOOK_DELAY)
        elif plain == 'v':
            self.step_look_h.cancel()
            self.step_look_v.step(maxangle, force, self.LOOK_DELAY)

    def handle(self, cmd, obj):
        if cmd == 'move':            
            self.move(obj['force'], obj['angle'])
        elif cmd == 'end':
            self.end(obj['what'])
        elif cmd == 'look':
            self.look(obj['plain'], obj['step'])
    
    def end(self, cmd):
        if cmd == 'move':
            self.move(0, 0)
        elif cmd == 'look':
            self.step_look_h.cancel()
            self.step_look_v.cancel()

    def enable(self):
        # TODO: implement
        print('joystick: ENABLE')
        self._enabled = True
        self.bot.Enable()

    def disable(self, reset_servos):
        # TODO: implement
        print('joystick: DISABLE')
        self._enabled = False
        self.bot.SetMotorDrive(pibot.M1, 0)
        self.bot.SetMotorDrive(pibot.M2, 0)
        self.step_left.cancel(True)
        self.step_right.cancel(True)
        self.step_look_h.cancel()
        self.step_look_v.cancel()
        if reset_servos:
            # FIXME: SERVO_H twitches after this sometimes
            # self.bot.SetServoControl(self.SERVO_H, RobotJoystick.angle_to_t(90))
            # self.bot.SetServoControl(self.SERVO_V, RobotJoystick.angle_to_t(90))
            # time.sleep(1)
            pass
        pibot.PiBot.Disable()

    @property
    def commands(self):
        return self.COMMANDS


class RobotWebsocketServer(object):
    JANUS = 'wss://localhost:{janus_ws_port}'
    CLOUD = 'wss://pi-gf.hldns.ru:{ws_port}/janus/pi/web/{id}'
    JOYSTICK = 'wss://pi-gf.hldns.ru:{ws_port}/joystick/pi/web/{id}'

    def __init__(self, joystick, camera, kwargs):
        self.JANUS = self.JANUS.format(**kwargs)
        self.CLOUD = self.CLOUD.format(**kwargs)
        self.JOYSTICK = self.JOYSTICK.format(**kwargs)
        self.ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        self.ssl_context.load_cert_chain(kwargs['ssl_cert'], kwargs['ssl_key'])
        self.start_server = websockets.serve(
            self.handle_ws, '0.0.0.0', kwargs['local_ws_port'], ssl=self.ssl_context)
        self.joystick = joystick
        self.camera = camera
        self.clients = 0

        self.client_ssl_context = ssl.SSLContext() #ssl.PROTOCOL_TLS_CLIENT)
        self.client_ssl_context.check_hostname = False
        self.client_ssl_context.verify_mode = ssl.CERT_NONE


    async def run(self):
        # FIXME: this causes "Task was destroyed but it is pending!"
        ws_twoway_task = asyncio.ensure_future(self.ws_2way(self.JANUS, self.CLOUD))
        ws_oneway_task = asyncio.ensure_future(self.ws_1way(self.JOYSTICK))
        done, pending = await asyncio.wait(
            [ws_twoway_task, ws_oneway_task],
            return_when=asyncio.FIRST_COMPLETED,
        )
        for task in pending:
            task.cancel()

    def handle_connect(self):
        if self.clients == 0:
            self.enable()
        self.clients += 1

    def handle_disconnect(self):
        if self.clients > 0:
            self.clients -= 1
            if self.clients == 0:
                self.disable()
    
    def enable(self):
        if self.joystick:
            self.joystick.enable()
        if self.camera:
            self.camera.enable()
    
    def disable(self):
        if self.joystick:
            self.joystick.disable(True)
        if self.camera:
            self.camera.disable()

    async def handle_ws(self, websocket, path):
        while True:
            try:
                msg = await websocket.recv()
                obj = json.loads(msg)
                cmd = obj['cmd']
                # print("< {}".format(obj))
                if cmd == 'hello':
                    print('client connected at '+path)
                    self.handle_connect()
                elif self.joystick and cmd in self.joystick.commands:
                    # print('joystick: '+cmd)
                    self.joystick.handle(cmd, obj)
                elif self.camera and cmd in self.camera.commands:
                    # print('camera: '+cmd)
                    self.camera.handle(cmd, obj)
            except websockets.exceptions.ConnectionClosed:
                print('client disconnected')
                self.handle_disconnect()
                break

    async def ws_relay(self, ws_from, ws_to):
        try:
            while True:
                message = await ws_from.recv()
                # print('>'+message)
                await ws_to.send(message)
        except:
            print('disconnected')

    async def ws_1way(self, cloud_addr):
        while True:
            try:
                async with  websockets.connect(cloud_addr, ssl=self.client_ssl_context) as cloud_ws:
                    print('1way: connected')
                    await self.handle_ws(cloud_ws, cloud_addr)
            except Exception as e:
                print('1way reconnecting')
                print(e)
                await asyncio.sleep(1)

    async def ws_2way(self, janus_addr, cloud_addr):
        while True:
            try:
                async with  websockets.connect(janus_addr, ssl=self.client_ssl_context, subprotocols=['janus-protocol']) as janus_ws, \
                        websockets.connect(cloud_addr, ssl=self.client_ssl_context) as cloud_ws:
                    print('2way: connected')
                    janus_task = asyncio.ensure_future(self.ws_relay(janus_ws, cloud_ws))
                    cloud_task = asyncio.ensure_future(self.ws_relay(cloud_ws, janus_ws))

                    done, pending = await asyncio.wait(
                        [janus_task, cloud_task],
                        return_when=asyncio.FIRST_COMPLETED,
                    )
                    for task in pending:
                        task.cancel()
            except Exception as e:
                print('2way reconnecting')
                print(e)
                await asyncio.sleep(1)


parser = argparse.ArgumentParser()
parser.add_argument("--disable-camera", help="Disable camera operation", action="store_true",
    default=False)
parser.add_argument("--disable-joystick", help="Disable joystick opration", action="store_true",
    default=False)
parser.add_argument("--disable-relay", help="Disable websocket relay", action="store_true",
    default=False)
parser.add_argument("--id", help="ID of this device",
    default='MAGIC')
parser.add_argument("--width", help="Camera image width",
    default=1920)
parser.add_argument("--height", help="Camera image height",
    default=1080)
parser.add_argument("--video-rtp-port", help="Video RTP output port (should match Janus input port)",
    default=8004)
parser.add_argument("--audio-rtp-out-port", help="Audio RTP output port (should match Janus input port)",
    default=8006)
parser.add_argument("--audio-rtp-in-port", help="Audio RTP input port (should match Janus output port)",
    default=8200)
parser.add_argument("--audio-in-device", help="ALSA input device number",
    default=1)
parser.add_argument("--audio-out-device", help="ALSA output deivce number",
    default=0)
parser.add_argument("--janus-ws-port", help="Janus control port",
    default=8989)
parser.add_argument("--ws-port", help="Websocket port",
    default=8989)
parser.add_argument("--local-ws-port", help="Websocket port for local server",
    default=8766)
parser.add_argument("--ssl-cert", help="X509 certificate for SSL",
    default='./ssl/ssl.crt')
parser.add_argument("--ssl-key", help="Private key for SSL",
    default='./ssl/ssl.key')
args = parser.parse_args()

if args.disable_camera:
    c = None    
else:
    c = RobotCamera(vars(args))

if args.disable_joystick:
    j = None
else:    
    j = RobotJoystick()

play_sound()

srv = RobotWebsocketServer(j, c, vars(args))
asyncio.get_event_loop().run_until_complete(srv.start_server)
if not args.disable_relay:
    asyncio.ensure_future(srv.run())
asyncio.get_event_loop().run_forever()
