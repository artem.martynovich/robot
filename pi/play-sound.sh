#!/bin/sh
gpio -g mode 22 out
gpio -g write 22 1
gst-launch-1.0 filesrc location=$1 ! wavparse ! audioconvert ! audioresample ! audio/x-raw,rate=48000,channels=2,format=S16LE ! alsasink device=hw:1
gpio -g write 22 0
