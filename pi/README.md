# Robot on Raspberry Pi

This folder contains configs and scripts only for Raspbian running on Raspberry Pi.

## Installing dependencies
* [Janus WebRTC Gateway](https://gitlab.com/artem.martynovich/janus-gateway): 
    1. follow the instructions to build and install it to the recommended location (`/opt/janus`). Enable only websocket transport and streaming plugin, everything else can be disabled.
    1. Edit [janus.transport.websockets.cfg](opt/janus/etc/janus/janus.transports.websockets.cfg): set SSL cert/key paths under `[certificates]` section.
    1. Edit [janus.cfg](opt/janus/etc/janus/janus.cfg): set the appropriate STUN and TURN server settings under the `[nat]` section. 
    1. Copy [configs](opt/janus/etc/janus) to `/opt/janus/etc/janus/`
    1. Copy [systemd startup script](lib/systemd/system) to `lib/systemd/system`
    1. Reboot. Make sure your TURN+STUN server is running before starting Janus, otherwise Janus won't use it until restarted.
* [Balena.io WiFi Connect](https://gitlab.com/artem.martynovich/wifi-connect):  
    `bash <(curl -L https://github.com/balena-io/wifi-connect/raw/master/scripts/raspbian-install.sh)`
* Python 3:
    
        sudo apt install python3 libboost-python-dev
        sudo pip install websockets
    Then, follow $1796248 to install [pibot library](https://gitlab.com/artem.martynovich/pibot).
* GStreamer: 

    `sudo apt install gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-tools python-gst-1.0`

## Run the main script

When all dependencies are installed, run in this directory:
`./robot.py`

This script controls servo motors, DC motors, camera and microphone streaming. It has a `--help` argument which describes all available options. You may want to set your own SSL cert/key paths. If the two-way proxy is not running (see [cloud/](../cloud)) add `--disable-relay`.

## Run the hotspot

Run `sudo ./start-wifi-connect`. This script brings up a WiFi AP on Pi and runs its web-based WiFi connection dialog. When connected to the Pi's own AP the Robot webpage is available at https://pi-robot.local or https://pi-robot or https://192.168.42.1. 
Once the user selects a hotspot and enters its password the AP goes down and Pi tries to connect to the selected hotspot.

`robot.py` can be launched before or after `start-wifi-connect`, doesn't matter.