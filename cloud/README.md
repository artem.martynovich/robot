# 2-way proxy for Robot #
This simple websocket proxy lets a web client and a Pi talk to each other behind NAT. It accepts incoming connections from both sides and forwards websocket messages both ways.

## How it works ##
Each participant opens a websocket connection to this proxy on `wss://<host>:8989/<service>/<from>/<to>/<id>`, where
* `<host>` is a DNS name or IP address of the machine where the proxy is running,
* `<service>` is a service identifier, can be anything, but currently used are "janus" and "joystick",
* `<from>` is the local identifier, e.g. "pi" or "web"
* `<to>` is the remote identifier, e.g. "pi" or "web"
* `<id>` is a device identifier.

Messages are proxied between matching `<service>/<from>/<to>/<id>` and `<service>/<to>/<from>/<id>`. For example, the script running on Pi creates two connections: `janus/pi/web/00112233` and `joystick/pi/web/00112233`. The web client also creates two connections: `janus/web/pi/00112233` and `joystick/web/pi/00112233`.

## How to run it ##

1. Install required node modules:

        npm install

2. Generate SSL certs if needed
3. Run the proxy:

        ./2way.js <ssl_cert> <ssl_key>

# TURN and STUN server

In order to use WebRTC behind NAT a TURN server is needed. Since we already have a cloud server running two-way proxy, why not run a TURN server on it? Also it includes STUN functionality needed to establish external IP address of WebRTC participants.

Installing and running is simple:

        sudo apt install turnserver
        sudo turnserver

The default configuration is fine as it is, and Janus configs in this repo are compatible with it.
