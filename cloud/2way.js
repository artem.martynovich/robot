#!/usr/bin/env node
const 	fs = require('fs'),
	WebSocket = require('ws'),
	https = require('https');

const WEB_PORT = 8989;

if(process.argv.length != 4) {
  console.error(`Usage: ${process.argv[0]} ${process.argv[1]} <ssl cert> <ssl key>`);
  process.exit(1);
}
const [_1,_2,ssl_crt,ssl_key] = process.argv;
const server = new https.createServer({
  cert: fs.readFileSync(ssl_crt),
  key: fs.readFileSync(ssl_key)
});

const wss = new WebSocket.Server({ server });

var ws_map = {};

wss.on('connection', (ws, con) => {
  const [_, service, who, other_who, id] = con.url.split('/');
  const which = service+'/'+who, other = service+'/'+other_who;
  ws.pi = {service, which, other, id};
  if(!ws_map[which])
    ws_map[which] = {};
  ws_map[which][id] = ws;
  console.log(`+ ${ws.pi.which} [${id}]`);

  ws.on('message', function incoming(message) {
    console.log(`> ${ws.pi.which} [${ws.pi.id}]: ${message}`);
    let ws_other = ws.pi.ws_other;
    if(!ws_other) {
      ws_other = ws_map[ws.pi.other] && ws_map[ws.pi.other][ws.pi.id];
      ws.pi.ws_other = ws_other;
    }
    if(ws_other) {
      console.log(`>> ${which} [${ws.pi.id}]: ${message}`);
      ws_other.send(message, function ack(error) {
        // If error is not defined, the send has been completed, otherwise the error
        // object will indicate what failed.
        if(error) {
          console.log(error);
          ws.close();
          delete(ws.pi.ws_other);
          delete(ws_map[which][ws.pi.id]);    
        }
      });
    }
  }).on('close', function close() {
    if(ws.pi) {
      console.log(`- ${which} [${ws.pi.id}]`);
      if(ws.pi.ws_other)
        ws.pi.ws_other.close();
      delete(ws.pi.ws_other);
      delete(ws_map[which][ws.pi.id]);
    }
  });
});

server.listen(WEB_PORT);
