# Web client for Robot

## How to start the web server

1. Install nginx
1. Generate SSL certs if needed
1. Link the proper version of web-config.json:

        cp public/web-config.<which>.json public/web-config.json

    When running the server on Pi, use "direct" version, i.e. web-config.direct.json. On the cloud server use "proxy" version.

1. Generate web files from the source:

        npm install
        npm run build
    
    The resulting files will be in build/ subdirectory.

1. Create nginx site, enable it and reload (run as root):
        
        m4 -D SSL_CERT=<ssl_cert> -D SSL_KEY=<ssl_key> -D PWD=<this dir>/build etc/nginx/sites-available/robot > /etc/nginx/sites-available/robot
        cp etc/nginx/sites-available/http-redirect /etc/nginx/sites-available/http-redirect
        ln -s /etc/nginx/sites-available/robot /etc/nginx/sites-enabled/robot
        ln -s /etc/nginx/sites-available/http-redirect /etc/nginx/sites-enabled/http-redirect
        service nginx reload
    
    Make sure no other sites are enabled on port 80. Nginx has a "default" site enabled upon fresh installation, so you may want to disable it:
        
        rm /etc/nginx/sites-enabled/default

    `http-redirect` is a dummy site which simply redirects clients from http://HOST to https://HOST. It is not required for proper functioning of the main site (`robot`).

## Developing the web app

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
